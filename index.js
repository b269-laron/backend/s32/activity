let http = require ("http");

http.createServer(function (request, response) {

	//http://localhost:4000/
	if ( request.url == "/" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to booking system')
	};

	//http://localhost:4000/profile
	if ( request.url == "/profile" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!')
	};

	//http://localhost:4000/courses
	if ( request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's our courses available")
	};

	//http://localhost:4000/addcourse
	if ( request.url == "/addcourse" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add a course to our resources")
	};

	//http://localhost:4000/updatecourse
	if ( request.url == "/updatecourse" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Update a course to our resources")
	};

	//http://localhost:4000/archivecourse
	if ( request.url == "/archivecourse" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources")
	};

}).listen(4000);
console.log ("Server running at localhost: 4000");

